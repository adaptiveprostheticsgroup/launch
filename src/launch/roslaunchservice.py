import rospy
from launch.srv import LaunchSrv

class ROSLaunchService(LaunchService):
    def __init__(self):
        rospy.wait_for_service("/launch")
        self.launch_srv = rospy.ServiceProxy("/launch", LaunchSrv)
        
    def launch_node(self, node_name, clazz, args=None, kwargs=None, header=None, force_restart=False):
        header = Header(stamp=header.stamp) if header else Header(stamp=rospy.Time.now())
        self.launch_srv.call(header, node_name, pickle.dumps(clazz),
                             pickle.dumps(args), pickle.dumps(kwargs), force_restart)