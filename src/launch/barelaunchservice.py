import rospy
from launch.launchservice import LaunchService
import multiprocessing as mp
from launch.srv import TerminateSrv

class BareLaunchService(LaunchService):
    
    class NodeWrapper(object):
        """"""
    
        #----------------------------------------------------------------------
        def __init__(self, name, clazz, args=[], kwargs={}):
            """Constructor"""
            
            self.name = name
            self.clazz = clazz
            self.args = args
            self.kwargs = kwargs
            
            self.process = None

    #---------------------------------------------    
    
    def __init__(self):
        super().__init__()
        #self.nodes = {}
        
    def launch_node(self, node_name, clazz, args=None, kwargs=None, header=None, force_restart=False):
        node = BareLaunchService.NodeWrapper(node_name, clazz, args, kwargs)
        node.process = mp.Process(name=node_name, target=launch, args=[node.clazz, node_name] + node.args, kwargs=node.kwargs)
        node.process.start()
        
        
def launch(clazz, name, *args, **kwargs):
    rospy.init_node(name)
    
    c = clazz(name, *args, **kwargs)
    def terminate(srv):    
        c.shutdown()    
    rospy.Service("%s/terminate" % name, TerminateSrv, terminate)
    

    
    rospy.spin()
    
    
    
    