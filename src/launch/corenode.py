import rospy
from launch.srv import TerminateSrv

########################################################################
class CoreNode(object):
    """"""

    #----------------------------------------------------------------------
    def __init__(self, name):
        """Constructor"""
        # rospy.init_node should not be called inside the node, it should be called outside
        self.name = name
        self.shutdown = False
        rospy.on_shutdown(self.on_shutdown)
        
    def on_shutdown(self):
        self.shutdown = True